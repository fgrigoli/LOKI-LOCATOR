#!/bin/bash
rm *.so *.o
gcc -c -I/opt/local/Library/Frameworks/Python.framework/Versions/3.4/include/python3.4m -I/opt/local/Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/numpy/core/include/ -O3 -fopenmp -lgomp location_py3_omp.c
gcc -bundle -flat_namespace -undefined suppress -fopenmp -lgomp -o location.so location_py3_omp.o
gcc -c -I/opt/local/Library/Frameworks/Python.framework/Versions/3.4/include/python3.4m -I/opt/local/Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/numpy/core/include/ -fopenmp -lgomp tt_processing_py3.c
gcc -bundle -flat_namespace -undefined suppress -fopenmp -lgomp -o tt_processing.so tt_processing_py3.o
gcc -c -I/opt/local/Library/Frameworks/Python.framework/Versions/3.4/include/python3.4m -I/opt/local/Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/numpy/core/include/ stalta_py3.c
gcc -bundle -flat_namespace -undefined suppress -o C_STALTA.so stalta_py3.o
