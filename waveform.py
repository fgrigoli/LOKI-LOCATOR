#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       Author: Francesco Grigoli


import os, sys
import numpy as num
import location
import C_STALTA
import tt_processing
from obspy.core   import read
from obspy.signal import filter
from scipy.signal import hilbert

class waveforms:

    def __init__(self, event_path, extension='*', comp=['E','N','Z']):
        if not os.path.isdir(event_path):
           print('Error: data path do not exist')     
        self.data_path = event_path
        self.extension = extension
        try:
            self.load_waveforms(event_path, comp)
        except:
            print('Error: data not read for the event: ', event_path)
           

    def load_waveforms(self, event_path, comp):
        files=os.path.join(event_path,self.extension)
        stream=read(files)
        self.station_list(stream)
        deltat=stream[0].stats.delta
        for tr in stream:
            if (deltat!=tr.stats.delta):
               print('Error!! All trace must have the same sampling rate')
               sys.exit()
        stream.detrend('demean')
        stream.detrend('linear')
        self.stream_x= stream.select(component=comp[0])
        self.stream_y= stream.select(component=comp[1])
        self.stream_z= stream.select(component=comp[2])
        self.deltat=deltat
        evid=str(stream[0].stats.starttime)
        evid=evid.replace(':', '')
        evid=evid.replace('-', '')
        self.evid=evid[0:15]

    def station_list(self, stream):
        data_stalist=[]
        self.ns=0
        for tr in stream:
            self.ns=max(tr.stats.npts,self.ns)
            if tr.stats.station not in data_stalist:
                data_stalist.append(tr.stats.station)
        self.data_stations=set(data_stalist)
    
    
    def filtering():
        print('prefilter not yet developed')

    def charfunc_s(self, xtr, ytr, epsilon):
        obs_dataH=num.zeros([self.nstation,self.ns])
        obs_dataH1=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        obs_dataH2=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        for j in range(self.nstation):
		             obs_dataH1[j,:]=hilbert((xtr[j,:]))
		             obs_dataH2[j,:]=hilbert((ytr[j,:]))
        obs_dataH1C=num.conjugate(obs_dataH1)
        obs_dataH2C=num.conjugate(obs_dataH2)
        for j in range(self.ns):
            for i in range(self.nstation):
                xx=obs_dataH1[i,j]*obs_dataH2C[i,j]
                xy=obs_dataH1[i,j]*obs_dataH2C[i,j]
                yx=obs_dataH2[i,j]*obs_dataH1C[i,j]
                yy=obs_dataH2[i,j]*obs_dataH1C[i,j]
                cov=num.array([[xx, xy],[yx, yy]])
                U, s, V = num.linalg.svd(cov, full_matrices=True)
                obs_dataH[i,j]=(s[0]**2)+epsilon
        return obs_dataH

    def charfunc_ps(self, xtr, ytr, ztr, epsilon):
        obs_dataH=num.zeros([self.nstation,self.ns])
        obs_dataV=num.zeros([self.nstation,self.ns])
        obs_dataH1=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        obs_dataH2=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        obs_dataH3=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        for j in range(self.nstation):
               obs_dataH1[j,:]=hilbert((ztr[j,:]))
               obs_dataH2[j,:]=hilbert((xtr[j,:]))
               obs_dataH3[j,:]=hilbert((ytr[j,:]))
        obs_dataH1C=num.conjugate(obs_dataH1)
        obs_dataH2C=num.conjugate(obs_dataH2)
        obs_dataH3C=num.conjugate(obs_dataH3)
        for j in range(self.ns):
            for i in range(self.nstation):
                xx=obs_dataH2[i,j]*obs_dataH2C[i,j]
                xy=obs_dataH2[i,j]*obs_dataH3C[i,j]
                xz=obs_dataH2[i,j]*obs_dataH1C[i,j]
                yx=obs_dataH3[i,j]*obs_dataH2C[i,j]
                yy=obs_dataH3[i,j]*obs_dataH3C[i,j]
                yz=obs_dataH3[i,j]*obs_dataH1C[i,j]
                zx=obs_dataH1[i,j]*obs_dataH2C[i,j]
                zy=obs_dataH1[i,j]*obs_dataH3C[i,j]
                zz=obs_dataH1[i,j]*obs_dataH1C[i,j]
                cov3d=num.array([[xx, xy, xz],[yx, yy, yz],[zx,zy,zz]])
                cov2d=num.array([[xx, xy],[yx, yy]])
                U2d, s2d, V2d = num.linalg.svd(cov2d, full_matrices=True)
                U3d, s3d, V3d = num.linalg.svd(cov3d, full_matrices=True)
                obs_dataV[i,j]=(s3d[0]**2)
                obs_dataH[i,j]=(s2d[0]**2)
        return obs_dataV, obs_dataH

    def charfunc_p(self, ztr, xtr, ytr):
        obs_dataV=ztr**2+xtr**2+ytr**2
        return obs_dataV

    def process_data(self, db_stations, epsilon=0.001):
        self.stations=self.data_stations & db_stations
        self.nstation=len(self.stations)
        self.xtr=num.zeros([self.nstation,self.ns,])
        self.ytr=num.zeros([self.nstation,self.ns,])
        self.ztr=num.zeros([self.nstation,self.ns,])
        xtr=num.zeros([self.nstation,self.ns,])
        ytr=num.zeros([self.nstation,self.ns,])
        ztr=num.zeros([self.nstation,self.ns,])
        for i,sta in zip(range(self.nstation),self.stations):
            trace_x=self.stream_x.select(station=sta)[0].data
            trace_y=self.stream_y.select(station=sta)[0].data
            trace_z=self.stream_z.select(station=sta)[0].data
            nsx=num.size(trace_x); nsy=num.size(trace_y); nsz=num.size(trace_z);         
            xtr[i,0:nsx]=self.stream_x.select(station=sta)[0].data
            ytr[i,0:nsy]=self.stream_y.select(station=sta)[0].data
            ztr[i,0:nsz]=self.stream_z.select(station=sta)[0].data
            xtr[i,:]=xtr[i,:]/num.max(num.abs(xtr[i,:]))
            ytr[i,:]=ytr[i,:]/num.max(num.abs(ytr[i,:]))
            ztr[i,:]=ztr[i,:]/num.max(num.abs(ztr[i,:]))
        obs_dataV=self.charfunc_p(ztr, xtr, ytr)
        obs_dataH=self.charfunc_s(xtr,ytr,epsilon)
        #obs_dataV, obs_dataH=self.charfunc_ps(xtr,ytr,ztr,epsilon)
        for j in range(self.nstation):
            obs_dataV[j,:]=obs_dataV[j,:]/num.max(obs_dataV[j,:])
            obs_dataH[j,:]=(obs_dataH[j,:]/num.max(obs_dataH[j,:]))+epsilon
        self.obs_dataV=obs_dataV
        self.obs_dataH=obs_dataH


    def recstalta(self, nshort, nlong, norm=1):
        tshort=nshort*self.deltat
        tlong=nlong*self.deltat
        ks=1./nshort
        kl=1./nlong
        obs_dataP=C_STALTA.recursive_stalta(tshort, tlong, self.deltat, self.obs_dataV, kl, ks, norm)
        obs_dataS=C_STALTA.recursive_stalta(tshort, tlong, self.deltat, self.obs_dataH, kl, ks, norm)
        return obs_dataP, obs_dataS