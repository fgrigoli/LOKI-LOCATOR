#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       Author: Francesco Grigoli


import os, sys
import numpy as num
import LatLongUTMconversion
import location
import C_STALTA
import tt_processing
from obspy.core   import read
from obspy.signal import filter
from scipy.signal import hilbert
import pylab as plt
from matplotlib.colors import Normalize

class traveltimes:
    
    def __init__(self, db_path, hdr_filename):
        if not os.path.isdir(db_path):
           print('Error: data or database path do not exist')
           sys.exit()
        self.db_path = db_path
        if not os.path.isfile(db_path+'/'+hdr_filename):
           print('Error: header file does not exist')
           sys.exit()
        self.hdr_filename = hdr_filename
        self.db_path=db_path
        self.load_header(self.db_path, self.hdr_filename)


    def load_header(self, db_path, hdr_filename):
        
        f = open(os.path.join(db_path, hdr_filename))
        lines = f.readlines()
        f.close()
        self.nx, self.ny, self.nz = [ int(x)   for x in lines[0].split()]
        self.x0, self.y0, self.z0 = [ float(x) for x in lines[1].split()]
        self.dx, self.dy, self.dz = [ float(x) for x in lines[2].split()]
        self.lat0, self.lon0 = [ float(x) for x in lines[3].split()]
        self.x = self.x0+(num.arange(0,self.nx)*self.dx)
        self.y = self.y0+(num.arange(0,self.ny)*self.dy)
        self.z = self.z0+(num.arange(0,self.nz)*self.dz)
        self.nxyz=self.nx*self.ny*self.nz
        db_stalist=[]
        
        for line in lines[4:]:
        	   db_stalist.append(line.split()[0])
        	
        self.db_stations=set(db_stalist)

    def load_traveltimes(self, phase, precision='single'):
        t={}
        for sta in self.db_stations:
            try:
               fn = os.path.join(self.db_path, 'layer.%(phase)s.%(station)s.time.buf' %{"phase":phase, "station":sta} )
            except:
               print('Error: reading file for station' + sta)
            if (precision=='single'):
                t[sta]= num.fromfile(fn, dtype=num.float32)
            elif (precision=='double'):
                t[sta]= num.fromfile(fn, dtype=num.float64)
            else:
                print('Error: precision must be set to "single" or "double"!!!')
                sys.exit()      
        return t

class waveforms:

    def __init__(self, event_path, extension='*', comp=['E','N','Z']):
        if not os.path.isdir(event_path):
           print('Error: data path do not exist')     
        self.data_path = event_path
        self.extension = extension
        try:
            self.load_waveforms(event_path, comp)
        except:
            print('Error: data not read for the event: ', event_path)
           

    def load_waveforms(self, event_path, comp):
        files=os.path.join(event_path,self.extension)
        stream=read(files)
        self.station_list(stream)
        deltat=stream[0].stats.delta
        for tr in stream:
            if (deltat!=tr.stats.delta):
               print('Error!! All trace must have the same sampling rate')
               sys.exit()
        stream.detrend('demean')
        stream.detrend('linear')
        self.stream_x= stream.select(channel=comp[0])
        self.stream_y= stream.select(channel=comp[1])
        self.stream_z= stream.select(channel=comp[2])
        self.deltat=deltat
        evid=str(stream[0].stats.starttime)
        evid=evid.replace(':', '')
        evid=evid.replace('-', '')
        self.evid=evid[0:15]

    def station_list(self, stream):
        data_stalist=[]
        self.ns=0
        for tr in stream:
            self.ns=max(tr.stats.npts,self.ns)
            if tr.stats.station not in data_stalist:
                data_stalist.append(tr.stats.station)
        self.data_stations=set(data_stalist)
    
    
    def filtering():
        print('prefilter not yet developed')

    def charfunc_s(self, xtr, ytr, epsilon):
        obs_dataH=num.zeros([self.nstation,self.ns])
        obs_dataH1=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        obs_dataH2=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        for j in range(self.nstation):
		             obs_dataH1[j,:]=hilbert((xtr[j,:]))
		             obs_dataH2[j,:]=hilbert((ytr[j,:]))
        obs_dataH1C=num.conjugate(obs_dataH1)
        obs_dataH2C=num.conjugate(obs_dataH2)
        for j in range(self.ns):
            for i in range(self.nstation):
                xx=obs_dataH1[i,j]*obs_dataH2C[i,j]
                xy=obs_dataH1[i,j]*obs_dataH2C[i,j]
                yx=obs_dataH2[i,j]*obs_dataH1C[i,j]
                yy=obs_dataH2[i,j]*obs_dataH1C[i,j]
                cov=num.array([[xx, xy],[yx, yy]])
                U, s, V = num.linalg.svd(cov, full_matrices=True)
                obs_dataH[i,j]=(s[0]**2)+epsilon
        return obs_dataH

    def charfunc_ps(self, xtr, ytr, ztr, epsilon):
        obs_dataH=num.zeros([self.nstation,self.ns])
        obs_dataV=num.zeros([self.nstation,self.ns])
        obs_dataH1=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        obs_dataH2=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        obs_dataH3=num.zeros([self.nstation,self.ns], dtype=num.complex64)
        for j in range(self.nstation):
               obs_dataH1[j,:]=hilbert((ztr[j,:]))
               obs_dataH2[j,:]=hilbert((xtr[j,:]))
               obs_dataH3[j,:]=hilbert((ytr[j,:]))
        obs_dataH1C=num.conjugate(obs_dataH1)
        obs_dataH2C=num.conjugate(obs_dataH2)
        obs_dataH3C=num.conjugate(obs_dataH3)
        for j in range(self.ns):
            for i in range(self.nstation):
                xx=obs_dataH2[i,j]*obs_dataH2C[i,j]
                xy=obs_dataH2[i,j]*obs_dataH3C[i,j]
                xz=obs_dataH2[i,j]*obs_dataH1C[i,j]
                yx=obs_dataH3[i,j]*obs_dataH2C[i,j]
                yy=obs_dataH3[i,j]*obs_dataH3C[i,j]
                yz=obs_dataH3[i,j]*obs_dataH1C[i,j]
                zx=obs_dataH1[i,j]*obs_dataH2C[i,j]
                zy=obs_dataH1[i,j]*obs_dataH3C[i,j]
                zz=obs_dataH1[i,j]*obs_dataH1C[i,j]
                cov3d=num.array([[xx, xy, xz],[yx, yy, yz],[zx,zy,zz]])
                cov2d=num.array([[xx, xy],[yx, yy]])
                U2d, s2d, V2d = num.linalg.svd(cov2d, full_matrices=True)
                U3d, s3d, V3d = num.linalg.svd(cov3d, full_matrices=True)
                obs_dataV[i,j]=(s3d[0]**2)
                obs_dataH[i,j]=(s2d[0]**2)
        return obs_dataV, obs_dataH

    def charfunc_p(self, ztr, xtr, ytr):
        obs_dataV=ztr**2+xtr**2+ytr**2
        return obs_dataV

    def process_data(self, db_stations, epsilon=0.001):
        self.stations=self.data_stations & db_stations
        self.nstation=len(self.stations)
        self.xtr=num.zeros([self.nstation,self.ns,])
        self.ytr=num.zeros([self.nstation,self.ns,])
        self.ztr=num.zeros([self.nstation,self.ns,])
        xtr=num.zeros([self.nstation,self.ns,])
        ytr=num.zeros([self.nstation,self.ns,])
        ztr=num.zeros([self.nstation,self.ns,])
        for i,sta in zip(range(self.nstation),self.stations):
            trace_x=self.stream_x.select(station=sta)[0].data
            trace_y=self.stream_y.select(station=sta)[0].data
            trace_z=self.stream_z.select(station=sta)[0].data
            nsx=num.size(trace_x); nsy=num.size(trace_y); nsz=num.size(trace_z);         
            xtr[i,0:nsx]=self.stream_x.select(station=sta)[0].data
            ytr[i,0:nsy]=self.stream_y.select(station=sta)[0].data
            ztr[i,0:nsz]=self.stream_z.select(station=sta)[0].data
            xtr[i,:]=xtr[i,:]/num.max(num.abs(xtr[i,:]))
            ytr[i,:]=ytr[i,:]/num.max(num.abs(ytr[i,:]))
            ztr[i,:]=ztr[i,:]/num.max(num.abs(ztr[i,:]))
        obs_dataV=self.charfunc_p(ztr, xtr, ytr)
        obs_dataH=self.charfunc_s(xtr,ytr,epsilon)
        #obs_dataV, obs_dataH=self.charfunc_ps(xtr,ytr,ztr,epsilon)
        for j in range(self.nstation):
            obs_dataV[j,:]=obs_dataV[j,:]/num.max(obs_dataV[j,:])
            obs_dataH[j,:]=(obs_dataH[j,:]/num.max(obs_dataH[j,:]))+epsilon
        self.obs_dataV=obs_dataV
        self.obs_dataH=obs_dataH


    def recstalta(self, nshort, nlong, norm=1):
        tshort=nshort*self.deltat
        tlong=nlong*self.deltat
        ks=1./nshort
        kl=1./nlong
        obs_dataP=C_STALTA.recursive_stalta(tshort, tlong, self.deltat, self.obs_dataV, kl, ks, norm)
        obs_dataS=C_STALTA.recursive_stalta(tshort, tlong, self.deltat, self.obs_dataH, kl, ks, norm)
        return obs_dataP, obs_dataS

class loki:


    def __init__(self, data_path, output_path, db_path, hdr_filename):
        self.data_path=data_path
        self.output_path=output_path
        self.db_path=db_path
        self.hdr_filename=hdr_filename
        self.data_tree, self.events=self.data_struct(self.data_path, self.output_path)      
            
    
    def data_struct(self, data_path, output_path):
        events=[]
        data_tree=[]
        for root,dirs,files in os.walk(data_path):
           if not dirs:
              data_tree.append(root)
              events.append(root.split('/')[-1])
        for event in events:
           if not os.path.isdir(output_path):
              os.mkdir(output_path)
        return data_tree, events


    def time_extractor(self, tp, ts, data_stations, db_stations):
        stations=data_stations & db_stations
        nsta=len(stations)
        nxyz= num.size(tp[list(stations)[0]])
        tp_mod=num.zeros([nxyz,nsta])
        ts_mod=num.zeros([nxyz,nsta])
        for i,sta in zip(range(nsta),stations): #use enumerate instead
            tp_mod[:,i]=tp[sta]
            ts_mod[:,i]=ts[sta]
        return tp_mod, ts_mod

    def catalogue_creation(self, event, lat0, lon0, ntrial, refell=23):
        (zorig,eorig,norig)=LatLongUTMconversion.LLtoUTM(refell, lat0, lon0) #da adeguare in python 3
        ev_file=self.output_path+'/'+event+'/'+event+'.loc'
        data=num.loadtxt(ev_file)
        if (ntrial>1):
           w=num.sum(data[:,4])
           xb= ((num.dot(data[:,1],data[:,4])/w)*1000)+eorig
           yb= ((num.dot(data[:,2],data[:,4])/w)*1000)+norig
           late,lone=LatLongUTMconversion.UTMtoLL(refell, yb, xb, zorig)
           zb= num.dot(data[:,3],data[:,4])/w
           cb= num.mean(data[:,4])
           cmax=num.max(data[:,4])
           merr=num.vstack((data[:,1],data[:,2],data[:,3]))
           err=num.cov(merr)
           errmax= num.sqrt(num.max(num.linalg.eigvals(err)))
        else:
           late,lone=LatLongUTMconversion.UTMtoLL(refell, (data[2]*1000)+norig, (data[1]*1000)+eorig, zorig)
           zb= data[3]; errmax='NA'; cb=data[4]; cmax=data[4];
        f=open(self.output_path+'/'+'catalogue','a')
        f.write(event+'    '+str(late)+'   '+str(lone)+'   '+str(zb)+'   '+str(errmax)+'   '+str(cb)+'   '+str(cmax)+'\n')
        f.close() 


    def location_process(self, extension='*', comp=['E','N','Z'], precision='single', *input):
        nshortmin=input[0]; nshortmax=input[1]; slrat=input[2]
        npr=input[3]
        ntrial=input[4]
        traveldb=traveltimes(self.db_path, self.hdr_filename)
        tp=traveldb.load_traveltimes('P', precision)
        ts=traveldb.load_traveltimes('S', precision)
        for event_path in self.data_tree:
            loc=waveforms(event_path, extension, comp)
            event=loc.evid
            print('accessing to the event folder: ', event_path, event)
            if os.path.isdir(self.output_path+'/'+event):
               continue
            else:
               os.mkdir(self.output_path+'/'+event)
            loc.process_data(traveldb.db_stations, epsilon=0.001)
            tp_mod, ts_mod=self.time_extractor(tp, ts, loc.data_stations, traveldb.db_stations)
            tp_mod, ts_mod=tt_processing.tt_f2i(loc.deltat,tp_mod,ts_mod, npr)
            for i in range(ntrial):
                gamma=num.random.random_sample()
                nshort=num.round(nshortmin*gamma+(1.-gamma)*nshortmax)
                nlong=nshort*slrat
                obs_dataP, obs_dataS=loc.recstalta(nshort, nlong) #da cambiare
                corrmatrix=location.stacking(tp_mod, ts_mod, obs_dataP, obs_dataS, npr)
                cmax=num.max(corrmatrix)
                corrmatrix=num.reshape(corrmatrix,(traveldb.nx,traveldb.ny,traveldb.nz))
                (ixloc,iyloc,izloc)=num.unravel_index(num.argmax(corrmatrix),(traveldb.nx,traveldb.ny,traveldb.nz))
                #xloc=ixloc*traveldb.dx; yloc=iyloc*traveldb.dy; zloc=izloc*traveldb.dz
                xloc=traveldb.x[ixloc]; yloc=traveldb.y[iyloc]; zloc=traveldb.x[izloc]
                out_file = open(self.output_path+'/'+event+'/'+event+'.loc','a')
                out_file.write(str(i)+' '+str(xloc)+' '+str(yloc)+' '+str(zloc)+' '+str(cmax)+' '+str(nshort)+' '+str(nlong)+'\n')
                out_file.close()
                num.save(self.output_path+'/'+event+'/'+'corrmatrix_trial_'+str(ntrial),corrmatrix)
                self.coherence_plot(self.output_path+'/'+event, corrmatrix, traveldb.x, traveldb.y, traveldb.z, i)
            self.catalogue_creation(event, traveldb.lat0, traveldb.lon0, ntrial)
        print('Ho finito!!!')


    def coherence_plot(self, event_path, corrmatrix, xax, yax, zax, ntrial, normalization=False):
        nx,ny,nz=num.shape(corrmatrix)
        CXY=num.zeros([ny, nx])
        for i in range(ny):
            for j in range(nx):
                CXY[i,j]=num.max(corrmatrix[j,i,:])
        
        CXZ=num.zeros([nz, nx])
        for i in range(nz):
            for j in range(nx):
			             CXZ[i,j]=num.max(corrmatrix[j,:,i])
    

        CYZ=num.zeros([nz, ny])
        for i in range(nz):
            for j in range(ny):
                CYZ[i,j]=num.max(corrmatrix[:,j,i])
        
        if normalization:
           nrm=Normalize(vmin=0., vmax=1.)
        else:
           nrm=None 
        

        fig = plt.figure()
        fig.suptitle('Coherence matrix X-Y', fontsize=14, fontweight='bold')
        ax = fig.gca()
        cmap = plt.cm.get_cmap('jet', 100)
        cs = plt.contourf(xax, yax, CXY, 20, cmap=cmap, interpolation='bilinear', norm=nrm)
        ax.set_xlabel('X (km)')
        ax.set_ylabel('Y (km)')
        cbar = plt.colorbar(cs)
        plt.axes().set_aspect('equal')
        plt.savefig(event_path+'/'+'Coherence_matrix_xy'+str(ntrial)+'.eps')


        fig = plt.figure()
        fig.suptitle('Coherence matrix X-Z', fontsize=14, fontweight='bold')
        ax = fig.gca()
        cmap = plt.cm.get_cmap('jet', 100)
        cs = plt.contourf(xax, zax, CXZ, 20, cmap=cmap, interpolation='bilinear', norm=nrm)
        ax.set_xlabel('X (km)')
        ax.set_ylabel('Z (km)')
        cbar = plt.colorbar(cs)
        ax.invert_yaxis()
        plt.axes().set_aspect('equal')
        plt.savefig(event_path+'/'+'Coherence_matrix_xz'+str(ntrial)+'.eps')


        fig = plt.figure()
        fig.suptitle('Coherence matrix Y-Z', fontsize=14, fontweight='bold')
        ax = fig.gca()
        cmap = plt.cm.get_cmap('jet', 100)
        cs = plt.contourf(yax, zax, CYZ, 20, cmap=cmap, interpolation='bilinear', norm=nrm)
        ax.set_xlabel('Y(km)')
        ax.set_ylabel('Z (km)')
        ax.invert_yaxis()
        cbar = plt.colorbar(cs)
        plt.axes().set_aspect('equal')
        plt.savefig(event_path+'/'+'Coherence_matrix_yz'+str(ntrial)+'.eps')
        plt.close("all")