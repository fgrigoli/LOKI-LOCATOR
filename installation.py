from distutils.core import setup, Extension
import numpy

# define the extension module
location = Extension('location', sources=['location_py3_omp.c'],
                          include_dirs=[numpy.get_include()],
                          extra_compile_args=['-O3','-fopenmp'],
                          extra_link_args=['-lgomp'])

tt_processing = Extension('tt_processing', sources=['tt_processing_py3.c'],
                          include_dirs=[numpy.get_include()],
                          extra_compile_args=['-O3','-fopenmp'],
                          extra_link_args=['-lgomp'])

C_STALTA = Extension('C_STALTA', sources=['stalta_py3.c'],
                          include_dirs=[numpy.get_include()],
                          extra_compile_args=['-O3'])

# run the setup
setup(ext_modules=[location,tt_processing,C_STALTA])
