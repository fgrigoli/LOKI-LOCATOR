#!/bin/bash
rm *.so *.o
gcc -c -I/usr/include/python3.5m -I/usr/lib/python3/dist-packages/numpy/core/include/numpy/ -O3 -fopenmp -lgomp location_py3_omp.c
gcc -bundle -flat_namespace -undefined suppress -fopenmp -lgomp -o location.so location_py3_omp.o
gcc -c -I/usr/include/python3.5m -I/usr/lib/python3/dist-packages/numpy/core/include/numpy/ -fopenmp -lgomp tt_processing_py3.c
gcc -bundle -flat_namespace -undefined suppress -fopenmp -lgomp -o tt_processing.so tt_processing_py3.o
gcc -c -I/usr/include/python3.5m -I/usr/lib/python3/dist-packages/numpy/core/include/numpy/ stalta_py3.c
gcc -bundle -flat_namespace -undefined suppress -o C_STALTA.so stalta_py3.o
