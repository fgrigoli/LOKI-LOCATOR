#!/bin/env python
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       Author: Francesco Grigoli
import timeit
import loki_mpi

#regular expression for file reading
extension='*'   #for instance '*.[she][hn][zne].gz'

#name of components
comp=['E','N','Z']

#precision of the travel-time database
precision='single'

# Path of the traveltime database
db_path='/Users/wulwife/Dropbox/LOKI_RELEASE/RUHR_TRAVELTIMES'
#db_path='/Volumes/Transcend/Vogtland/Traveltime/Vogt_syn_time_interp'

# name of the file containing information about the grid parameters
hdr_filename='header.hdr'

# Path of of the data folder or event folder (for single event location)
#data_path='/Volumes/Transcend/Vogtland/Vogtland_synthetic/unperturbed/1969-12-31_184746.752'
data_path='/Users/wulwife/Dropbox/LOKI_RELEASE/e0269.169.07'

# Path where store the outputs (catalogues, coherence matrices etc.)
output_path='/Users/wulwife/test_C_MPI'

ntrial=1 			         # bootstrap relocation (for multiple solutions)
slrat=2.0  			       # short to long time window ration 
nshortmax=20 			     # maximum length of the short time window 
nshortmin=20 		      # minimum length of the short time window 
npr=1  			           # number of parallel cores
epsilon=0.001		      # to avoid numerical issues

start = timeit.default_timer()
loc=loki_mpi.loki_mpi(data_path, output_path, db_path, hdr_filename)
loc.location_process(extension, comp, precision, nshortmin, nshortmax, slrat, npr, ntrial)
stop = timeit.default_timer()
print('computing time', stop-start)